import React from "react";
import "./topbar.css";
import { Language, NotificationsNone, Settings } from "@material-ui/icons";
export default function Topbar() {
  return (
    <div className="topbar">
      <div className="topbarWrapper">
        <div className="topLeft">
          <span className="logo">welcome</span>
        </div>
        <div className="topRight">
          <div className="topbarIconsContainer">
            <NotificationsNone />
            <span className="topIconBadge">2</span>
          </div>
          <div className='topbarIconsContainer'>
          <Language/>
<span className='topIconBadge'>2</span>
        </div>
        <div className='topbarIconsContainer'>
          <Settings/>
        </div>
        <img src="https://images.pexels.com/photos/16606640/pexels-photo-16606640.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1" alt="" className="topAvatar"/>
        </div>
      </div>
    </div>
  );
}
