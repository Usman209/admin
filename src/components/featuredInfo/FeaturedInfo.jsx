import { ArrowDownward, ArrowUpward } from "@material-ui/icons";
import React from "react";

import "./featuredInfo.css";

export default function FeaturedInfo() {
  return (
    <div className="featured">
      <div className="featuredItem">
        <span className="featuredTitle">Revanue</span>

        <div className="featuredMonyContainer">
          <span className="featuredMony">$2,345</span>
          <span className="featuredMonyRate">
            $2,340 <ArrowDownward />
          </span>
        </div>

        <span className="featuredSub">Compared to last month</span>
      </div>

      <div className="featuredItem">
        <span className="featuredTitle">Revanue1</span>

        <div className="featuredMonyContainer">
          <span className="featuredMony">$2,345</span>
          <span className="featuredMonyRate">
            $2,340 <ArrowDownward className="featuredIcon negative" />
          </span>
        </div>

        <span className="featuredSub">Compared to last month</span>
      </div>

      <div className="featuredItem">
        <span className="featuredTitle">Revanue2</span>

        <div className="featuredMonyContainer">
          <span className="featuredMony">$2,345</span>
          <span className="featuredMonyRate">
            $2,340 <ArrowUpward className="featuredIcon" />
          </span>
        </div>

        <span className="featuredSub">Compared to last month</span>
      </div>
    </div>
  );
}
